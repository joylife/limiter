"use strict";


var Limiter = require("../limiter");

var limiter = new Limiter({
	limitdUrl: "limitd://localhost:9231",
	console: console
})

var callback = function (err) {
	if (err) {
		console.log(err);
		process.exit(1);
	}

	process.exit(0);
};

var withdrawls = [
	["emails", process.argv[2], parseInt(process.argv[3])],
	["emailRate", process.argv[2], parseInt(process.argv[3])]
];

var withdrawlsComplete = [];
limiter.transferAll(withdrawls).then(() => callback(), callback);

