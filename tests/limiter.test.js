const Limiter = require('../limiter');

const PUT_FAILED = "Failed to put!";
const TAKE_FAILED = "Failed to take!";

const TEST_BUCKETS = {
    emails: {
        size: 10
    },
    emailRate: {
        size: 10,
        per_hour: 10
    }
}
/*
    The limitd service is essentially a token bank
    that we can withdraw from (take) or deposit to (put)
*/
const limiterWithMockService = (succeeds = true, conforms = true) => {
    let token_pile = 0;
    const limiter = new Limiter({ console: console, limitdUrl: 'localhost:6379', buckets: TEST_BUCKETS });

    limiter._limitdRedis = (new class MockLimitd {
        constructor(should_succeed, should_conform) {
            this.should_succeed = should_succeed;
            this.should_conform = should_conform;
        }
        put(type, id, amount, callback) {
            console.log("called put", type, id, amount);
            console.log(this.should_succeed)
            if(!this.should_succeed){
                return callback(new Error(PUT_FAILED), "This is not falsey");
            }
            token_pile += amount;
            callback(null, {})
        }
        take(type, id, amount, callback) {
            console.log("called take", type, id, amount);
            if(!this.should_succeed){
                return callback(new Error(TAKE_FAILED), null);
            }else if (!this.should_conform) {
                return callback(null, {}); // non coformant
            }
            token_pile -= amount;
            callback(null, { conformant: true })
        }
        _num_tokens(){ return token_pile }
    }(succeeds, conforms))
    return limiter;
}

test("Resolves to null if we aren't doing a real transfer", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", 0, null)
        .then(result => {
            expect(result).toBe(null);
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});

test("Adds tokens if provided a negative token amount", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", -5)
        .then(result => {
            expect(result).toMatchObject({});
            expect(limiter._limitdRedis._num_tokens()).toBe(5);
            done();
        })
});

test("Removes tokens if provided a positive token amount", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", 5)
        .then(result => {
            expect(result).toMatchObject({"conformant": true});
            expect(limiter._limitdRedis._num_tokens()).toBe(-5);
            done();
        })
});

test("Reverts all taken tokens when revert is called", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", 5)
        .then(result => {
            expect(result).toMatchObject({"conformant": true})
            expect(limiter._limitdRedis._num_tokens()).toBe(-5);
        })
        .then(() => limiter.revert())
        .then(() => {
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});

test("Reverts all given tokens when revert is called", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", -5)
        .then(result => {
            expect(result).toMatchObject({})
            expect(limiter._limitdRedis._num_tokens()).toBe(5);
        })
        .then(() => limiter.revert())
        .then(() => {
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});

test("Reverts a series of actions when revert is called", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", -5)
        .then(result => {
            expect(result).toMatchObject({})
            expect(limiter._limitdRedis._num_tokens()).toBe(5);
        })
        .then(() => limiter.transfer("a", "b", 2))
        .then(result => {
            expect(result).toMatchObject({"conformant": true})
            expect(limiter._limitdRedis._num_tokens()).toBe(3);
        })
        .then(() => limiter.revert())
        .then(() => {
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});

test("Properly executes a transferAll()", done => {
    let limiter = limiterWithMockService();
    limiter.transferAll([
        ["a", "b", -5],
        ["a", "b",  2]
    ])
        .then(result => {
            expect(result).toMatchObject([{}, {"conformant": true}])
            expect(limiter._limitdRedis._num_tokens()).toBe(3);
        })
        .then(() => limiter.revert())
        .then(() => {
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});

test("Errors from the underlying service are propagated (put)", () => {
    let limiter = limiterWithMockService(succeeds=false);
    let endsInFailure = limiter.transfer("a", "b", -5);
    return expect(endsInFailure).rejects.toThrow(PUT_FAILED);
});

test("Errors from the underlying service are propagated (take)", () => {
    let limiter = limiterWithMockService(succeeds=false);
    let endsInFailure = limiter.transfer("a", "b", 5);
    return expect(endsInFailure).rejects.toThrow(TAKE_FAILED);
});

test("Non-conforming response results in an error", () => {
    let limiter = limiterWithMockService(succeeds=true, conforms=false);
    let endsInFailure = limiter.transfer("a", "b", 5);
    return expect(endsInFailure).rejects.toThrow("Non Conformant");
});

test("An operation that fails is not logged", (done) => {
    let limiter = limiterWithMockService();
    limiter.transfer("emailRate", "b", -5)
        .then(result => {
            expect(result).toBe(null)
            expect(limiter._limitdRedis._num_tokens()).toBe(5);
            limiter.succeeds = false; // somebody is playing games
        })
        .then(() => limiter.transfer("emailRate", "b", 2))
        .catch(() => "failed uniquely!") // do nothing!
        .then(result => {
            expect(result).toBe("failed uniquely!")
            expect(limiter._limitdRedis._num_tokens()).toBe(5);
            limiter.succeeds = true; // for real this time
        })
        .then(() => limiter.revert())
        .then(() => {
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});

test("Properly handles spamming revert() calls", done => {
    let limiter = limiterWithMockService();
    limiter.transfer("a", "b", -5)
        .then(result => {
            expect(result).toMatchObject({})
            expect(limiter._limitdRedis._num_tokens()).toBe(5);
        })
        .then(() => limiter.transfer("a", "b", 2))
        .then(result => {
            expect(result).toMatchObject({"conformant": true})
            expect(limiter._limitdRedis._num_tokens()).toBe(3);
        })
        .then(() =>
            Promise.all([
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
                limiter.revert(),
            ])
        )
        .then(() => {
            expect(limiter._limitdRedis._num_tokens()).toBe(0);
            done();
        })
});
