"use strict";


var LimitdRedis = require("limitd-redis");// name to redis matter

const defaultBuckets = {
	emailsByEventId: { size: 3000 },
	emailsByEventIdRate: { size: 500, per_hour: 500 },
	emailsByEventId_Warn: { size: 1000 },
	emailsByEventIdRate_Warn: { size: 200, per_hour: 200 },
	eventsByUserId: { size: 5, per_hour: 5 },
	eventsByIpRate: { size: 240, per_hour: 240 },
	paymentAttempts: { size: 9000 },
	paymentAttemptsByReceiverId: { size: 900 },
	// for development testing
	testPerMinute: { size: 3, per_minute: 1 },
};

const defaultRedisKeyPrefix = 'limitdRedis:';

class Operation {
	constructor({opname,type,id,amount}){
		this.opname = opname;
		this.type = type;
		this.id = id;
		this.amount = amount;
	}
	clone() {
		return new Operation({
			opname: this.opname,
			type: this.type,
			id: this.id,
			amount: this.amount,
		});
	}
}

// global singleton
let connection = null;

class Limiter {

	constructor(config) {
		this._console = config.console;
this._config = config;
		this._operationList = [];
		this._limitdRedis = this.connect();
	}

	/*
		The LimitD Client will auto-connect,
			and we invoke this method from the Constructor.
			stub it to prevent a real connect from happening under Test Suite Conditions
	*/
	connect() {
		let buckets = this._config.buckets || {};
		buckets = {
			...defaultBuckets,
			...buckets
		}
		const keyPrefix = this._config.keyPrefix || defaultRedisKeyPrefix;
		if(!connection) {
			connection = new LimitdRedis({
				uri: this._config.limitdUrl,
				buckets,
				keyPrefix
			});
		}
		this._limitdRedis = connection;
		return connection;
	}

	/*
		Go through the log of things we did and do the opposite
	*/
	revert() {
		let ops = this._operationList.map(op => op.clone());
		this._operationList = [];

		let p = Promise.resolve();
		for (let op of ops) {
			switch (op.opname) {
				case 'put':
					p = p.then(() => this._take(op.type, op.id, op.amount));
					break;
				case 'take':
					p = p.then(() => this._put(op.type, op.id, op.amount));
					break;
			}
		}
		return p;
	}

	// just add something to the operations log
	record(op) {
		this._operationList.push(op);
	}

	// promise wrapper for limitd lib put
	_put (type, id, amount) {
		return new Promise((resolve, reject) =>
			this._limitdRedis.put(type, id, amount, (err, resp) => {
				if (err) {
					this._console.error(err);
					return reject(err);
				}
				this.record(new Operation({opname: "put",type,id,amount}));
				resolve(resp);
			})
		);
	}

	// promise wrapper for limitd lib take
	_take (type, id, amount) {
		return new Promise((resolve, reject) =>
			this._limitdRedis.take(type, id, amount, (err, resp) => {
				if (err) {
					this._console.error(err);
					return reject(err);
				}

				if (!resp.conformant) {
					this._console.error(`Cannot transfer ${amount} from ${type}-${id}`);
					return reject(new Error("Non Conformant"));
				}

				this.record(new Operation({opname: "take",type,id,amount}));
				return resolve(resp);
			})
		)
	}

	transfer(type, id, amount) {
		if (typeof amount === "undefined" || amount === null) {
			amount = 1;
		}

		this._console.info(`Transfering ${amount} from ${type}-${id}`);
		// If amount == 0, there is nothing to do
		if (amount == 0) { return Promise.resolve(null); }
		if (amount < 0) { return this._put(type, id, -amount); }
		return this._take(type, id, amount);
	}

	transferAll(arr) {
		return Promise.all(
			arr.map(spec => this.transfer(spec[0], spec[1], spec[2]))
		)
	}
}


module.exports = Limiter;
