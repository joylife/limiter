### A wrapper for limitd and limitdctl

##### Why ?

Provides utilities and rollback functionality for our clients.

### Testing:

`npm run test` will run offline tests using a mock.
`npm run sanity-check` will run against a limitd service configured to run with the config at `/tests/limitd.config`.

##### How can I run that limitd instance quickly ?

check [the Wiki](https://withjoy.atlassian.net/wiki/spaces/KNOW/pages/1905557728/Running+withjoy+limitd+locally)

run this in the root of this repo:
```bash
    docker run -p 9231:9231 \
    --mount \
        type=bind,source="$(pwd)"/tests/limitd.config,target=/etc/limitd.config \
    -it joylife/limitd:release-6-d5e4805
```
